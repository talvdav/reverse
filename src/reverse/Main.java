package reverse;

public class Main {

	public Main() {
		// TODO Auto-generated constructor stub
	}
	public static int OrderOfMagnitude (int input) {
		return (int)Math.log10(input);
	}
	public static void main(String[] args) {
		//@author "Thomas A. Alvarado Davalos - talvdav@mailbox.org
		// TODO Auto-generated method stub
		// TODO Check if input exceeds max integer value
        //          2147483647		
		int input = 2147483647;
		int temp = input;
		int help = 0;
		long rev = 0;
		int mag = OrderOfMagnitude(input);
		
		while (mag >=0 ) {
			//System.out.println("mag: " + mag);
			help = temp % 10;
			//System.out.println("help: " + help);
			temp = temp / 10;
			//System.out.println("temp: " + temp);
			if (mag != 0)
				rev += help * (long)Math.pow(10,mag);
			else
				rev += temp + help;
			//System.out.println("rev: " + rev);
			mag = OrderOfMagnitude(temp);
		
			//System.out.println("----------------------------");
		}
		System.out.println("input: " + input);
		System.out.println("rev:   " + rev);
		if (rev < 0) {
		System.out.println("Revers number exceeds 2147483647");
		System.out.println("hence WRONG resault!!!");
		}
	}
}
